#!/bin/bash

set -e

args=""
for arg in "$@"; do
    [ "$arg" = "-d" ] && args="$args -f .docker/docker-compose.debug.yml"
    [ "$arg" = "-p" ] && args="$args -f .docker/docker-compose.proxy.yml"
done

docker-compose -p zms --env-file=.env -f .docker/docker-compose.base.yml $args run node bash scripts/npm_watch.sh
