#!/usr/bin/env bash

set -e

cd /zmslocaldev/repos
for dir in *; do
    cd $dir
    if [ -f "composer.json" ]; then
        echo -e "\e[93m\e[4m\e[1mRepository: $dir\e[39m\e[0m" # fancy colors
        composer install --no-scripts
    fi
    cd ..
done
