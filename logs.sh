#!/bin/bash

set -e

docker-compose -p zms --env-file=.env -f .docker/docker-compose.base.yml logs -f zms
